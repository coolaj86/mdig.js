mdig.js
=======

| [dns-suite.js](https://git.coolaj86.com/coolaj86/dns-suite.js)
| [dig.js](https://git.coolaj86.com/coolaj86/dig.js)
| **mdig.js**
| [digd.js](https://git.coolaj86.com/coolaj86/digd.js)
| Sponsored by [Daplie](https://daplie.com).

multicast dig, in JavaScript (ES5.1)

Create and capture mDNS query and response packets to disk as binary and/or JSON.
Options are similar to the Unix `dig` command, but for mDNS.

Install with git
-------

```bash
# Install the latest of v1.x
npm install -g 'git+https://git.coolaj86.com/coolaj86/mdig.git#v1'
```

```bash
# Install exactly v1.0.0
npm install -g 'git+https://git.coolaj86.com/coolaj86/mdig.git#v1.0.0'
```

Install without git
-------

Don't have git? Well, you can also bow down to the gods of the centralized, monopolized, concentrated, dictator-net
(as we like to call it here at Daplie Labs), if that's how you roll:

```bash
npm install -g mdig
```

Usage
-----

### Format

```bash
mdig [TYPE] <query>
```

### Example

```bash
mdig PTR _services._dns-sd._udp.local
```

### defaults

When run with no options `mdig` will run with these as the default:

```bash
mdig -p 5353 @224.0.0.251 PTR _services._dns-sd._udp.local +time=3
```

### Non-standard mDNS

For debugging you may use a non-standard port and address

```bash
# non-standard port
mdig -p 1900

# non-standard broadcast address
mdig @239.255.255.250
```

### Common Service Types

```
_ssh._tcp.local
_sftp-ssh._tcp.local"
_nfs._tcp.local
_pdl-datastream._tcp.local
_printer._tcp.local
_ipp._tcp.local
_http._tcp.local
_apple-mobdev2._tcp.local
_workstation._tcp.local
_udisks-ssh._tcp.local
```

Options
-------

```
--debug
--output <path/to/file>     write query and response(s) to disk with this path prefix (ex: ./samples/dns)
-t <type> (superfluous)     default ANY (mdns default: PTR)
-c <class>                  default IN
-p <port>                   default 53 (mdns default: 5353) (listener is random for DNS and 5353 for mDNS)
-q <query> (superfluous)    required (ex: daplie.com)

+time=<seconds>             Sets the timeout for a query in seconds.
```
